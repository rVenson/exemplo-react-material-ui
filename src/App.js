import React from "react";
import Header from "./components/Header";
import UserPage from "./components/User/UserPage";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { Container, Box } from "@material-ui/core";

function App() {
  return (
    <Box>
      <Header></Header>
      <Container>
        <BrowserRouter>
          <Switch>
            <Route path="/users" component={UserPage} />
          </Switch>
        </BrowserRouter>
      </Container>
    </Box>
  );
}

export default App;
