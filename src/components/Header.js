import React from 'react'
import {Button, Toolbar, AppBar, Typography} from '@material-ui/core'

export default class Header extends React.Component{
    render(){
        return(
        <AppBar position="static">
        <Toolbar>
            <Typography variant='h5'>Menu CRUDzinho</Typography>
            <Button color="inherit" href="/users">Usuários</Button>
            <Button color="inherit" href="/games">Jogos</Button>
            </Toolbar>
        </AppBar>
        )
    }
}