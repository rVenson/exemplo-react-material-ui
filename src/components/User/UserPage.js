import React from 'react'
import UserForm from './UserForm'
import UserList from './UserList'
import {Box} from '@material-ui/core'

export default class UserPage extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            users: [
                { username: "rvenson", password: '123456', admin: true}
            ]
        }
    }

    addUser = (user) =>{
        this.setState({
            users: [...this.state.users, user]
        })
    }

    render(){
        return(
            <Box display="flex" flexDirection='column'>
                <UserForm onSubmit={this.addUser}></UserForm>
                <UserList users={this.state.users}></UserList>
            </Box>
        )
    }
}