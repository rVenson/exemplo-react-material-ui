import React from 'react'
import {Button, TextField, Checkbox, FormControl, Box} from '@material-ui/core'

export default class UserForm extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            username: "",
            password: "",
            admin: false
        }
    }

    onChange = (event) => {
        this.setState({
            [event.target.id]: event.target.value
        })
    }

    onChangeCheckbox = (event) => {
        this.setState({
            [event.target.id]: event.target.checked
        })
    }

    onSubmit = (event) => {
        event.preventDefault()
        
        const isValidInput = this.validate()

        if(isValidInput){
            this.props.onSubmit(this.state)

            this.setState({
                username: "",
                password: "",
                admin: false
            })
        }
    }

    validate = () => {
        if(this.state.username.length >= 3 && this.state.password.length >= 3){
           return true 
        }
    }

    render(){
        return(
                <FormControl style={{margin: '3rem'}}>
                    <TextField type="text" autoComplete='off' value={this.state.username} id="username" placeholder="Usuário" onChange={this.onChange}></TextField>
                    <TextField type="password" autoComplete='off' value={this.state.password} id="password" placeholder="Senha" onChange={this.onChange}></TextField>
                    <div><Checkbox type="checkbox" checked={this.state.admin} id="admin" onChange={this.onChangeCheckbox}></Checkbox> Administrador</div>
                    <Button onClick={this.onSubmit} variant="contained" color="primary">Inserir</Button>
                </FormControl>
        )
    }
}