import React from 'react'
import {Table, TableHead, TableBody, TableCell, TableRow} from '@material-ui/core'

export default class UserList extends React.Component{

    constructor(props){
        super(props)
    }

    render(){
        var userList = this.props.users.map((value, indice) => {
            return <TableRow key={indice}>
                <TableCell>{indice + 1}</TableCell>
                <TableCell>{value.username}</TableCell>
                <TableCell>{value.password}</TableCell>
                <TableCell>{value.admin.toString()}</TableCell>
            </TableRow>
        })

        return(
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>#</TableCell>
                        <TableCell>Nome</TableCell>
                        <TableCell>Senha</TableCell>
                        <TableCell>Admin</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {userList}
                </TableBody>
            </Table>
        )
    }
}